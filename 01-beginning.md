# Install prerequisites
## Install erlang and elixir
To install erlang and elixir, you can use the [official install guides](https://elixir-lang.org/install.html) for your platform. 

If you'd like the ability to install multiple versions of elixir/erlang, and switch between them, there is a great tool for that called [asdf](https://github.com/asdf-vm/asdf)
Follow the instructions to install asdf on your machine. Then add the elixir and erlang plugins, and install the specific versions we'll use for this tutorial:
```bash
asdf plugin-add erlang
asdf install erlang 21.2.2
asdf global erlang 21.2.2 # Activate in global space
asdf plugin-add elixir
asdf install elixir 1.7.4
asdf global elixir 1.7.4 # Activate in global space
```
You can check the currently used versions by running:
```
$ asdf current
elixir         1.7.4    (set by /home/$USER/.tool-versions)
erlang         21.2.2   (set by /home/$USER/.tool-versions)
```

## Install Hex
A package manager for elixir (similar to `gem` for Ruby, or `npm` for Node.js). You can learn more about Hex from this talk: https://youtu.be/cbCnTKVLuu8
To install or upgrade Hex, run:
```
mix local.hex
```
## Explain mix?

## Install Phoenix
Phoenix is an MVC web framework written in Elixir. It makes creating a web server in Elixir very easy, while remaining quite lightweight and performant. You can read more about Phoenix in [the docs](https://hexdocs.pm/phoenix/overview.html).

First, install a mix task for creating new Phoenix projects from the command line:
```
mix archive.install hex phx_new 1.4.0
```
You can uninstall any previous version you might have with:
```
mix archive.uninstall phx_new
```
To get to know the `phx.new` task a little better and see the different options you can use, check out [the docs](https://github.com/phoenixframework/phoenix/blob/master/installer/lib/mix/tasks/phx.new.ex).

# Create new Phoenix project
Now that we have everything install, we can create our first Phoenix project as a basis for this tutorial. Start by running:
```
mix phx.new ./api-server --module ApiServer --app api_server --umbrella --database postgres --no-webpack --no-html --binary-id
```
- `./api-server` directory for the app
- `--module ApiServer` name of the main module (PascalCase dirname is used by default)
- `--app api_server` name of the OTP app (Whats OTP?); lowercase, underscored
- `--umbrella` create project as one "umbrella" app with multiple child apps
- `--database postgres` (default)
- `--no-webpack` don't include JS/CSS files (this is only an API server)
- `--no-html` don't include HTML templates
- `--binary-id` string UUID instead of autoincrement integer

The command will ask you if you want to fetch dependencies right away. You can say yes, but you can also do it manually by running:
```
mix deps.get
```
Note: you have to run this command every time your dependencies change. For our umbrella app, we can run it on the root level, and it will fetch dependencies for all our child apps.

## Look around
Explain basic files
- `mix.exs`
  - apps_path: path to our child apps
  - deps: we can leave empty
- `config/*`
  - common config for all apps
- `apps/api_server_web` app for the web interface
  - `mix.exs`
    - `application` how to run this app
      - `extra_applications` apps we need but don't have in deps
    - `deps` dependencies for this app
      - `in_umbrella` apps from ourt umbrella app
  - `config/config.exs`
    - notice our :binary_id setting
  - `config/*`
    - nothing interesting
  - `lib/api_server_web.ex` main module for our web app
    - global config for all controllers/views/routers/channels
  - `lib/api_server_web/application.ex` application callback module
    - starts endpoint module
  - `lib/api_server_web/endpoint.ex`
    - uses macros (socket, plug)
    - modifies behavior for all request before it sends them to router
  - `lib/api_server_web/router.ex` main router for the web app
    - pipeline: common actions(plugs) grouped in a named pipeline
    - scope: group a subset of URIs
  - `channels,controllers,views` to describe
- `apps/api_server` app for core functionality
  - `config/(dev|prod|test)` database connections config
  - `lib/api_server.ex`
  - `lib/api_server/application.ex` starts a Repo (connection to the DB)
  - `lib/api_server/repo.ex` connection to the DB

# Create a dummy route
Focus on `apps/api_server_web`, this is the app that will be responsible for handling requests to our server.

Open `lib/api_server_web/router.ex`, and add our first route inside of the `/api` scope. We'll point it to `index` action of the ApiController (we will create it next).
```elixir
scope "/api", ApiServerWeb do
  pipe_through :api

  get "/", Controllers.ApiController, :index
end
```

Create the controller in `lib/api_server_web/controllers/api_controller.ex`
```elixir
defmodule ApiServerWeb.Controllers.ApiController do
  use ApiServerWeb, :controller # will include the controller part from api_server_web.ex

  def index(conn, _params) do
    json(conn, %{message: "Hello World!"})
  end

end
```

## Start the server and test our first route
To start our server, you need to run the following command (back in the root of our umbrella app):
```
mix phx.server
```
Right now, the server will complain about failing to connect to the DB. You can ingore this for now, or you can comment out the `ApiServer.Repo` in `apps/api_server/lib/api_server/application.ex` like this:
```elixir
def start(_type, _args) do
  children = [
    # ApiServer.Repo
  ]

  Supervisor.start_link(children, strategy: :one_for_one, name: ApiServer.Supervisor)
end
```
Now you can try going to `localhost:4000/api` and you should see our JSON response. Congratulations!

