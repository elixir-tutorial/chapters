# Add Absinthe to our project
Absinthe is a hex package for working with [GraphQL](https://graphql.org/) in elixir applications. You can check the documentation [here](https://hexdocs.pm/absinthe/overview.html).

To add Absinthe to our project, change the deps in `apps/api_sever_web/mix.ex` to include following dependencies:
```elixir
defmodule ApiServerWeb.MixProject do
  # ... some code omitted
  defp deps do
    [
      # ... some code omitted
      {:absinthe, "~> 1.4"},
      {:absinthe_plug, "~> 1.4"},
      {:absinthe_phoenix, "~> 1.4"}
    ]
  end
  # ... some code omitted
end
```
Now, to fetch the dependencies, run `mix deps.get` in the root of our umbrella app

## Absinthe schema
GraphQL (and therefore Absinthe) is schema driven. GraphQL schema describes what kind of requests our GraphQL endpoint will handle, the types and fields of our data.

Let's create a file for our schema: `apps/api_server_web/lib/api_server_web/schema.ex`

```elixir
defmodule ApiServerWeb.Schema do

  use Absinthe.Schema

  query do
    @desc "Test endpoint"
    field :health, :string do
      resolve(fn (_parent,_args,_resolution) -> {:ok, "alive"} end)
    end
  end

end
```
Let's discuss what we are doing here. First off, every GraphQL endpoint has three root level fields:
- `query` - for getting data from the API
- `mutation` - for sending updates to the API
- `subscription` - for subscribing to real time updates

In our first example, we only define the `query` root level field, with one nested field inside it.

To define a custom field, we are using the `field` macro from Absinthe, and passing in the name of the name of the field (`:health`) and its type. We can also provide some documentation for the field in the `@desc` macro. GraphQL has vary powerful dev tools so a good documentation will be of great help to consumers of our API.

To define what happens when someone requests our field, we are providing a resolver function with the following arguments:
- `_parent` for nested queries, we get the object returned from the parent query
- `_args` arguments used while requesting this field
- `_resolution` struct that holds the global information about the request resolution

(Note: right now we're not using any of them, hecne the `_`)

## Add graphql endpoints
To view our schema, we need to add an endpoint that will point to it. We will also create an endpoint with [GraphiQL](https://github.com/graphql/graphiql), one of the great dev tools I mentioned. It will help explore our API while we work on it.

In `apps/api_server_web/lib/api_server_web/router.ex` change the `scope "/api"` section to look like this:

```elixir
scope "/api" do
  pipe_through(:api)

  get("/", ApiServerWeb.Controllers.ApiController, :index)

  forward("/graphql", Absinthe.Plug, schema: ApiServerWeb.Schema)
  forward("/graphiql", Absinthe.Plug.GraphiQL, schema: ApiServerWeb.Schema)
end
```
Notice we got rid of the `scope "/api", ApiServerWeb`. This way we can access the Absinthe module (otherwise elixir will look for ApiServerWeb.Absinthe module instead).

Now we should have our graphql api available on /api/graphql, and the GraphiQL api explorer on /api/graphiql

Run `mix phx.server` to star the server, visit `localhost:4000/api/graphiql` and try for yourself.
The query we made possible is:
```graphql
query {
  health
}
```
Click around a bit and see if you can find the doc string we created for our field.

## Query our users
To have our code a bit more organized, we will be putting a schema for each of our resources into a separate file uder a `schema` directory.
Create the dir and a file `apps/api_server_web/lib/api_server_web/schema/user_types.ex`
In this file, we will define our user object for Absinth schema.
```elixir
defmodule ApiServerWeb.Schema.UserTypes do
  use Absinthe.Schema.Notation

  object :user do
    @desc "User UUID"
    field(:id, :id)

    @desc "User email address"
    field(:email, :string)

    @desc "User first name"
    field(:first_name, :string)

    @desc "User last name"
    field(:last_name, :string)
  end

end
```

Now, in `apps/api_server_web/lib/api_server_web/schema.ex` we need to import the user type we just created and create a field for it:
```elixir
defmodule ApiServerWeb.Schema do
  # ... some code omitted
  import_types ApiServerWeb.Schema.UserTypes
  alias ApiServerWeb.Resolvers

  query do
    # ... some code omitted
    @desc "All users"
    field :users, list_of(:user) do
      resolve(&Resolvers.Users.list_users/3)
    end
  end
end
```
Notice we didn't use a simple anonymous function for the users resolver. Instead, it is a good practice to define your resolvers in a separate module. Let's create it right now in
`apps/api_server_web/lib/api_server_web/resolvers/users.ex`:
```elixir
defmodule ApiServerWeb.Resolvers.Users do
  def list_users(_parent, _args, _res) do
    {:ok, ApiServer.Users.list_users()}
  end
end
```
The resolver is using the `ApiServer.Users` module, specifically its `list_users` method. It's a common practice to have the `User` module (singular) defining our user model, and a `Users` model (plural) for defining methods for querying users.

Create `apps/api_server/lib/api_server/users.ex`:
```elixir
defmodule ApiServer.Users do
  alias ApiServer.Repo # For connection to our DB
  alias ApiServer.User # Our user DB schema
  
  def list_users() do
    User
    |> Repo.all()
  end
end
```
Note: If you haven't done this previously, now is a good time to uncomment `ApiServer.Repo` in `apps/api_server/lib/api_server/appliaction.ex`, since we are using it now.

And that's it! Now if you restart your server (`mix phx.server`) and go to the `/api/graphiql` endpoint, you should be able to run the users query:
```graphql
query {
  users {
    id
    email
    firstName
    lastName
  }
}
```