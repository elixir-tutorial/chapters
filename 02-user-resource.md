# Setup local postgres server
For running our database, we'll be using PostgreSQL docker image. If you don't have docker, first check out the [install guide](https://docs.docker.com/install/)
After that, download the [postgres](https://hub.docker.com/_/postgres) image and run it with:
```bash
docker pull postgres:latest
docker run --name api-server-db -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=api_server_dev -p 127.0.0.1:5432:5432 -d postgres
```

# Create Users Schema
For communicationg with the DB, elixir uses the `Ecto` package. Each resource we want to use in our app (e.g. users) needs to have a corresponding Ecto schema.
To create a schema for the user model, we will use this mix task inside the `apps/api_server_web/` directory
```
mix phx.gen.schema User users email:string:unique
```
Here's what the arguments do (you can find more info [here](https://hexdocs.pm/phoenix/Mix.Tasks.Phx.Gen.Schema.html)):
- `User` the name of the module holding the schema
- `users` name of the database table
- `email:string:unique` a field to add to the schema

The generator will create 2 files for us. We will have a closer look at these files while adding 2 more fields to our schema.

## User module:
`apps/api_server/lib/api_server/user.ex`

First, let's have a look at the file. If you are interested in how the `Ecto` module works, you can also have a look at [the docs](https://hexdocs.pm/ecto/Ecto.html#content)
```elixir
defmodule ApiServer.User do
  use Ecto.Schema  # Used to define the mapping between DB and elixir
  import Ecto.Changeset # Used to validate and transform incoming data before saving to DB


  @primary_key {:id, :binary_id, autogenerate: true} # Options for the primary/foreign keys
  @foreign_key_type :binary_id # Notice the :binary_id is used, as we defined while creating our project
  schema "users" do # Definition of the database table
    field :email, :string # Just one field for now

    timestamps() # Helper to create automatically populated `inserted_at` and `updated_at` timestamp fields
  end

  # method for creating and updating users with raw data (all user provided data needs to go through here)
  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email]) # take fields from the input attrs
    |> validate_required([:email]) # validate the presence of required fields
    |> unique_constraint(:email) # check for uniqueness of email
  end
end
```

Now, let's add new fields for our users' first and last name:
```elixir
defmodule ApiServer.User do
# ... some code omitted
  schema "users" do
    field :email, :string
    field :first_name, :string
    field :last_name, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :first_name, :last_name])
    |> validate_required([:email, :first_name, :last_name])
    |> unique_constraint(:email)
  end

  # For convinience, we will add one more method to build user from pure attrs
  @doc false
  def build(attrs) do
    changeset(%__MODULE__{}, attrs)
  end
end
```


## The migration file
`apps/api_server/priv/repo/migrations/{timestamp}_create_users.exs`

The migration file is an elixir script (`.esx` - can be ran without compiling), which takes care of adding the neccesary tables, fields and indexes to the database (or removing them, when we decide to revert the migration).
```elixir
defmodule ApiServer.Repo.Migrations.CreateUsers do
  use Ecto.Migration # module from Ecto SQL with helpers for create migrations

  # Defines a symmetric change in the database (both to apply and roll-back the migration)
  def change do 
    create table(:users, primary_key: false) do # Create table for users, don't use the default primary key
      add :id, :binary_id, primary_key: true # Custom definition for our primary key, as defined when creating our project
      add :email, :string # Simple string field

      timestamps() # Again the automatic timestamps `inserted_at` and `updated_at`
    end

    create unique_index(:users, [:email]) # Create the unique index for email field
  end
end

```
The `change` method is used for changes in the DB that are reversible (the migration has enough information to both create the table/index and remove them). For more complicated (and asymmetric) migrations, you can use the `up` and `down` methods. Read more in [the docs](https://hexdocs.pm/ecto_sql/Ecto.Migration.html).

While adding the fields for users' first and last name, we'll also change the type of the `:id` field to `:uuid` (PostgreSQL has native support for uuid's, so it will be more efficient)
```elixir
defmodule ApiServer.Repo.Migrations.CreateUsers do
  # ... some code omitted
    create table(:users, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :email, :string
      add :first_name, :string
      add :last_name, :string

      timestamps()
    end
  # ... some code omitted
  end
end
```

## Add some initial users
To have the DB populated with some testing entries right away, we can modify the `apps/api_serverapi_server/priv/repo/seeds.exs` file. This is an elixir script used to seed the database.
```elixir
alias ApiServer.User
alias ApiServer.Repo

%User{email: "filip@example.com", first_name: "Filip", last_name: "Vavera"} |> Repo.insert!
%User{email: "viktor@example.com", first_name: "Viktor", last_name: "Nawrath"} |> Repo.insert!
```

## Let's migrate our database
In `apps/api_server`, run:
```
mix ecto.setup
```
This is actually a shortcut for running 3 tasks:
- `ecto.create` - Create the database if it doesn't exist yet
- `ecto.migrate` - Perform all missing migrations
- `run priv/repo/seeds.exs` - Run our seeds script
